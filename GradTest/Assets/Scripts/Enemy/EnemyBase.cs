﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))] //Make sure that there is a navmesh agent attached to the enemy
public class EnemyBase : MonoBehaviour //Script controlling the behaviour of enemy 1 (The enemy that always slowly moves towards the player)
{
    protected NavMeshAgent agent; //Reference to the navmesh agent of the enemy
    public GameObject player; //Reference to the player, used for calculating distance for beeping and pathfinding
    public GameManager gameManager; //Reference to the gamemanager so that player lives can be decreased
    public float movementSpeed = 1F; //The speed that this enemy moves at, as it is intended for it to move slowly, it has a speed of 1
    public string playerTag = "Player"; //Tag given to the player so that it can be identified
    public GameObject sfxPrefab;//Prefab for playing sfx clips
    public AudioClip radarBeep; //The sound clip that this enemy plays
    protected Vector3 respawnPoint; //Point that the enemy goes back to when the player dies
    private float distanceToPlayer;//The distance to the player
    private float startingDistance; //The distance the enemy starts from the player at the start of the game
    public float maxBeepTime = 3.5F;//The maximum gap between radar beeps
    private void Awake()
    {
        agent = gameObject.GetComponent<NavMeshAgent>(); //Get the navmesh agent component
        agent.speed = movementSpeed; //Set the movement speed
        respawnPoint = transform.position;//Set the point that this enemy goes back to when the player dies
        startingDistance = Vector3.Distance(transform.position, player.transform.position); //Calculate the "Max Distance" from the player, used for the beeping
        StartCoroutine(Beep()); //Starts the beeping
    }

    private void Update()
    {
        SeekPlayer(); //This enemy is always following the player, so we should check where they currently are and go there
    }

    protected void SeekPlayer() //Protected so that it can be used by children of this class
    {
        agent.SetDestination(player.transform.position); //Sets the navmesh agent's destination to the player's current destination

    }

    private void OnCollisionEnter(Collision collision) //When the enemy collides with something
    {
        if (collision.gameObject.CompareTag(playerTag))
        { //Check if it was the player
            gameManager.RemovePlayerLife(); //Trigger the player attacked function
            Respawn();//Place them back at the starting point
        }
    }

    private void PlaySoundEffect() //Method for playing sound at the location
    {
        GameObject sfx = Instantiate(sfxPrefab, transform); //Spawn the sound effect prefab
        AudioSource source = sfx.GetComponent<AudioSource>();
        source.clip = radarBeep; //Sets the clip for the audio source
        source.Play(); //Plays it
        Destroy(sfx, source.clip.length); //Destroys the spawned sfx from the level when it is finished
    }

    protected void Respawn() //Moves the enemy back to the starting point so that the player isn't instagibbed
    {
        transform.position = respawnPoint;
    }

    IEnumerator Beep() //Continously Beeps
    {
        float nextBeepTime = (GetDistanceToPlayer() / startingDistance) * maxBeepTime;//How long till the next beep
        PlaySoundEffect(); //Play this beep
        yield return new WaitForSeconds(nextBeepTime); //Wait enough time for the next beep
        StartCoroutine(Beep()); //Do it all again
    }

    protected float GetDistanceToPlayer() //Gets the distance from the enemy to the player
    {
        return (Vector3.Distance(player.transform.position, transform.position));
    }
}
