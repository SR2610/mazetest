﻿using UnityEngine;
using UnityEngine.UI;

public class PickupObject : MonoBehaviour //Script attached to the player's camera to pickup and drop items
{
    public float raycastRange = 4.0F; //Default range of the raycast
    public Text toolTip; //Reference to the tooltip that instructs the player
    private GameObject interactedObject;//The object that the player is either currently looking at or holding
    public Transform holdPosition; //The position that the player will have an object when they are holding it
    private bool holdingItem; //If they are currently holding an item
    public LayerMask pickupMask; //Layermask to show what layers the raycast will hit, and what layers get ignored by the raycast.  Used to stop the raycast hitting the containers so that balls can be removed

    private void Start()
    {
        holdingItem = false; //They aren't holding an item when they start
    }

    private void Update()
    {
        if (Input.GetButtonDown("Interact")) //If they press the key to pick up or drop
        {
            if (holdingItem) //If they are holding something, drop it
            {
                DropItem();

            }
            if (interactedObject != null) //If they are looking at something they can pick up, pick it up
            {

                interactedObject.GetComponent<BallInteractable>().OnPickup(holdPosition); //Tell the item its been picked up
                holdingItem = true;
            }
        }

        if (holdingItem)
        {
            toolTip.text = ("Press F to Drop Ball"); //Show the tooltip to drop the item if they are holding one
            return; //If they are holding an item, we don't need to be doing the raycast
        }

        bool Hit = Physics.Raycast(transform.position, transform.forward, out RaycastHit HitInfo, raycastRange, pickupMask); //Perform the raycast

        if (Hit) //If the raycast hit something
        {
            GameObject hitObj = HitInfo.transform.gameObject; //The object hit by the raycast
            if (hitObj.GetComponent<BallInteractable>()) //Check if it is a ball
            {

                interactedObject = hitObj; //If it is, set the interacted object and the tooltip
                toolTip.text = ("Press F to Pickup Ball");
            }
            else //If its not, there is no interacted object
                interactedObject = null;

        }
        if (interactedObject == null) //If we are not looking at an object, hide the tooltip
            toolTip.text = "";

    }

    public void DropItem()//Called when the player is attacked
    {
        if (interactedObject != null&& interactedObject.GetComponent<BallInteractable>()) //Check if they are holding an object
            interactedObject.GetComponent<BallInteractable>().OnDrop(); //Tell the items it has been droped
        interactedObject = null;
        holdingItem = false;
    }
}
