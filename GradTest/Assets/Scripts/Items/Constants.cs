﻿public static class Constants //Constants used for level generation
{
    public static int MAX_BALLS_IN_ROOM = 1; //Maximum number of balls that can be in a room
    public static int MAX_YELLOW_CONTAINERS_IN_ROOM = 1; //Maximum number of yellow containers that can be in a room
    public static int MAX_RED_CONTAINERS_IN_ROOM = 1; //Maximum number of red containers that can be in a room
}
