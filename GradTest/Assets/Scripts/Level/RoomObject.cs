﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomObject : MonoBehaviour //An object to define a room on the map
{
    [Header("Room Settings")]
    public Vector2 roomSize = new Vector2(10, 10); //The size of the room, room sizes in the maze level are 10 x 10
    public float tolerance = 2F;  //used to bring the spawnable room size down.  This prevents objects spawning inside the walls or from potentially blocking the entrance of the door

    private int ballsInRoom, yellowContainersInRoom, redContainersInRoom = 0; //Ints counting the number of each type of item spawned

    public bool TryPlaceObjectInRoom(GameObject objectToPlace, GameManager.ObjectTypes objectType) //Trys to place an object in the room based off of the rules
    {
        switch (objectType)
        {
            case GameManager.ObjectTypes.BALL:
                if (ballsInRoom >= Constants.MAX_BALLS_IN_ROOM) //Checks if we already have a ball in the room
                    return false;
                ballsInRoom++;
                break;
            case GameManager.ObjectTypes.YELLOW_CONTAINER: //Checks for a yellow container already in the room
                if (yellowContainersInRoom >= Constants.MAX_YELLOW_CONTAINERS_IN_ROOM)
                    return false;
                yellowContainersInRoom++;
                break;
            case GameManager.ObjectTypes.RED_CONTAINER: //Checks for a red container already in the room
                if (redContainersInRoom >= Constants.MAX_RED_CONTAINERS_IN_ROOM)
                    return false;
                redContainersInRoom++;
                break;
        }
        PlaceObject(objectToPlace,objectType); //If it gets here, it can place the object in the room
        return true;
    }



    private void PlaceObject(GameObject objectToPlace, GameManager.ObjectTypes objectType) //Places the object in the room.  Assumes this room object is in the center of the room on the map.
    {
        float xPos = Random.Range(-(roomSize.x - tolerance) / 2, (roomSize.x - tolerance) / 2); //Gets a random x position in the bounds.
        float zPos = Random.Range(-(roomSize.y - tolerance) / 2, (roomSize.y - tolerance) / 2); //Gets a random z position in the bounds.
        GameObject spawnedObject = Instantiate(objectToPlace, transform); //Spawn the object
        spawnedObject.transform.localPosition = new Vector3(xPos, (objectType == GameManager.ObjectTypes.BALL ? 0.25F : 0), zPos); //Sets its position within the room
    }

    private void OnDrawGizmos() //Shows the area that the room will spawn objects within
    {
        Gizmos.color = new Color(1, 0, 0, 0.5f);
        Gizmos.DrawCube(transform.position, new Vector3(roomSize.x - tolerance, 1, roomSize.y - tolerance));
    }
}
