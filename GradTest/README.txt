The goal of this game is to avoid the two types of enemies and escape the maze. In order to escape the maze, the user must place 3 red balls in the red containers, and 3 yellow balls in the yellow containers, which will then unlock the exit of the maze. 

To Do
- Location of the red tubes should load be randomized every time the game is run.
- Location of the yellow tubes should be randomized every time the game is run.
- Location of the red balls and yellow balls should be randomized every time the game is run. 
- After randomization, no room should contain more than one ball. 
- After randomization, it is okay for a room to contain both a red and yellow container, but no room should contain two containers of the same colour.
- The player can pick up and drop the red and yellow balls (but can only pick up one at a time)
- The player can not pick up or move containers. 
- Your objective is to get to the black door at the end of the maze, and go through it to escape.
- This door should remain locked until you place 3 red balls into the red containers, and 3 balls into the yellow containers. It will then unlock and allow the player to pass through.
- If you try and walk through the door before you have placed the balls in the tubes, it should display a message displaying how many correct balls have been placed in the tubes (E.g 0/3 red. 2/3 yellow)
- You can choose to have doors open automatically when a player/enemy pass through them, or you can make it so the player must press a button to open them.
- Doors should close behind you when you walk through
- The player has 3 lives.
- If the enemy reaches the player, the player is instantly sent back to the start room and they lose a life.
- If the player loses all 3 lives, they lose the game. (No need for fancy end game screens. Simply just display "Game over" or something very very basic)

Enemy 1
- Should move very slowly
- Always moves towards the player 
- This enemy should emit a sound (any sound clip will do) that becomes more frequent as they get closer to the player (Like the radar sound in Alien)

Enemy 2
- Moves faster than the first enemy 
- This enemy should have a patrolling state and should chase the player when the player gets close, and then return to patrolling if the player gets far enough away (you choose the distances)
- This enemy should periodically change the path he patrols (Should change every 30 seconds)


*IMPORTANT*
- Everything you need should be in the Prefabs folder (except the first person controller). 
- The Maze scene is in the Scenes folder
- The scene hierarchy is purposely quite messy, so feel free to modify it as you need.
- You can use a pre-made FPS controller if you like, (e.g the FPS controller from the Unity Standard Assets), but please write your own pick up script.
- Please feel free to reuse any code you have used before in your modules or personal projects if you feel it fits
- Be creative. If you want to add anything that you think will make this already amazing game better, then feel free to do so, but please make reference to it when you submit the game. (Keep this within reason. E.g add a particle effect when you pick something up. Dont go adding weapons or anything crazy)

I know it is frustrating being asked to do assignments in your own time, so we have asked to give you a full week to complete it so you have the option to do a small bit here and there. Nothing listed above should be too difficult individually, however when combined with other parts, there may be some tricky parts, and there may be some small things that need some thought.
If you have any questions, or any parts are not clear, feel free to email Jamesvia JamesB@nsccreative.com

