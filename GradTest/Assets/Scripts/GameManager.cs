﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour //Script to manage the core game logic
{
    [Header("Prefabs")]//All the prefabs that need to be spawned in at the start of the game.
    public GameObject redTube;
    public GameObject yellowTube;
    public GameObject redBall;
    public GameObject yellowBall;

    [Header("Player Related")]
    public Transform startPoint;
    public GameObject player;

    [Header("Values")]
    public int itemsToGenerate = 3; //The number of red balls (and containers) to generate.
    public int maxPlayerLives = 3;//Number of lives the player starts with
    private int currentPlayerLives; //The number of lives the player currently has.

    public List<RoomObject> rooms = new List<RoomObject>(); //List of all the rooms in the level
    private float hitCooldown = 0;

    private int yellowCorrect, redCorrect; //Variables counting the number of correct red and yellow balls in containers

    [Header("User Interface")]
    public GameObject panel; //The panel containing the two text elements, so that they can be turned off and on
    public Text redBallText;
    public Text yellowBallText;
    public Text livesText; //Text for the current player lives
    [HideInInspector]
    public bool panelShown;

    public enum ObjectTypes //The three types of object that are randomly generated in the level
    {
        BALL, YELLOW_CONTAINER, RED_CONTAINER
    }

    public enum ObjectColours //Colours for the balls and the containers
    {
        YELLOW, RED
    }

    private void Start() //Called when the level has loaded
    {
        GenerateItemsInLevel();
        redCorrect = 0;
        yellowCorrect = 0;
        currentPlayerLives = maxPlayerLives; //At the start of the game, the player should have maximum lives
        UpdatePanel();
        panelShown = false;
        FormatPlayerLives();
    }

    private void FormatPlayerLives() //Updates the life display of the player
    {
        livesText.text = string.Format("Lives: {0}", currentPlayerLives);
    }

    private void GenerateItemsInLevel() //Generates all the items in level
    {
        int numberOfRooms = rooms.Count; //Gets the number of rooms that have been set up

        bool generated = false;
        for (int i = 0; i < itemsToGenerate; i++) //Loops to three to place all the items
        {
            while (!generated) //Continuously tries to generate the item in an avalible room, does the same for all items
                generated = rooms[Random.Range(0, numberOfRooms)].TryPlaceObjectInRoom(redBall, ObjectTypes.BALL); //Generates 3 red balls in random rooms
            generated = false;
            while (!generated)
                generated = rooms[Random.Range(0, numberOfRooms)].TryPlaceObjectInRoom(yellowBall, ObjectTypes.BALL); //Generates 3 yellow balls
            generated = false;
            while (!generated)
                generated = rooms[Random.Range(0, numberOfRooms)].TryPlaceObjectInRoom(yellowTube, ObjectTypes.YELLOW_CONTAINER); //Generates 3 yellow containers
            generated = false;
            while (!generated)
                generated = rooms[Random.Range(0, numberOfRooms)].TryPlaceObjectInRoom(redTube, ObjectTypes.RED_CONTAINER); //Generates 3 red containers
            generated = false;
        }

    }
    private void Update()
    {
        if (hitCooldown > 0) //Decrease the cooldown on the player taking damage
            hitCooldown -= Time.deltaTime;


        panel.SetActive(panelShown); //Set the ball panel to be displayed or not
        if (panelShown)
            UpdatePanel();
    }

    public void RemovePlayerLife() //Called when one of the enemies makes contact with the player
    {
        if (hitCooldown <= 0) //A cooldown to prevent the player losing two lifes at once
        {
            hitCooldown = 1F; //Give them a second of invunrability
            player.GetComponentInChildren<PickupObject>().DropItem();//Drop a ball if they are holding one
            player.transform.SetPositionAndRotation(startPoint.position, startPoint.rotation);//Send the player back to the start of the level
            currentPlayerLives--; //Remove a life from the player
            FormatPlayerLives(); //Update life display
            if (currentPlayerLives <= 0) //If the player's life number hits zero or below, they reach gameover state
            {
                CrossSceneSettings.didWin = false;//Set the static didWin variable to false to show the correct end screen
                SceneManager.LoadScene(1); //Load the game over scene

            }
        }
    }

    public bool CanFinalDoorUnlock() //Checks that the right number of red and yellow balls are in the containers
    {
        return ((yellowCorrect == itemsToGenerate) && (redCorrect == itemsToGenerate));
    }

    public void BallPlaced(ObjectColours ballColour) //Called when a ball is placed in a container
    {
        switch (ballColour) //Check what colour of ball it is
        {
            case ObjectColours.YELLOW:
                yellowCorrect++; //Increment the correct counter
                break;
            case ObjectColours.RED:
                redCorrect++;
                break;
        }
    }

    private void UpdatePanel()
    {
        redBallText.text = string.Format("Red: {0} / {1}", redCorrect, itemsToGenerate);
        yellowBallText.text = string.Format("Yellow: {0} / {1}", yellowCorrect, itemsToGenerate);

    }

}
