﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreenControl : MonoBehaviour //Used for reloading the game when the player wants to.  Displays both victory and failure screens
{
    [Header("Text Objects")]
    public Text mainText; //References to the text objects so that they can be changed
    public Text secondaryText;
    [Header("End Scene Strings")]
    public string winHeading = "Victory"; //Texts displayed on both the victory and game over screens
    public string winSecondary = "You escaped the maze!";
    public string loseHeading = "Game Over";
    public string loseSecondary = "You ran out of lives.";


    private void Awake() //Set the correct texts
    {

        Cursor.lockState = CursorLockMode.None; //Unlock the cursor from the middle of the screen
        Cursor.visible = true; //Show the cursor

        if (CrossSceneSettings.didWin) //Check the static cross scene class to find what condition they met and what will be displayed
        {
            mainText.text = winHeading;
            secondaryText.text = winSecondary;
        }
        else
        {
            mainText.text = loseHeading;
            secondaryText.text = loseSecondary;
        }

    }

    public void ReloadGame() //Reloads the maze level
    {
        SceneManager.LoadScene(0);
    }
}


public static class CrossSceneSettings //Seperate class for passing information between the two scenes
{
    public static bool didWin = true; //Variable saying if the player won or lost
}
