﻿using UnityEngine;

public class Container : MonoBehaviour //Script to control the behavior of the containers
{

    public GameManager.ObjectColours containerColour; //The colour of the container, used to tell if the right ball is in the right container
    public string gameManagerName = "GameManager"; //The name of the game manager
    private GameManager gameManagerRef;//Reference to the game manager
    private bool isFilled = false; //Bool to show if the container has already been filled, prevents all the balls going in one container


    private void Awake()
    {
        gameManagerRef = GameObject.Find(gameManagerName).GetComponent<GameManager>(); //Gets a reference to the gamemanager at runtime
    }

    private void OnTriggerStay(Collider other) //When an object (the ball) enters the container
    {
        if (!isFilled && other.GetComponent<BallInteractable>())//Check if it is a ball that has gone in and that we don't already have one
        {
            BallInteractable ball = other.GetComponent<BallInteractable>();//Get a reference to the interactable component as it is needed twice
            if (!ball.isHeld && ball.ballColour == containerColour)//Check that the ball is the right colour and that it isn't being held
            {
                gameManagerRef.BallPlaced(ball.ballColour);//Tell the game manager that the ball has been placed
                isFilled = true;//Set this to filled so no new balls will count
                ball.gameObject.SetActive(false); //Disable the ball so that it can't be picked up again
            }
        }

    }
}
