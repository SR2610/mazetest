﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPatrolling : EnemyBase //Script controlling the behaviour of the patrolling enemy
{

    public List<PatrolRoute> routes = new List<PatrolRoute>(); //List of patrol routes that the enemy can follow
    private PatrolRoute currentRoute; //The route this enemy is currently following
    private int currentPatrolPoint; //Variable for the current point on the patrol route that the enemy is on
    private float internalTimer; //Internal timer, used for detecting how long until they need to stop patrolling a route and switch
    public float timeBetweenSwitching = 30F;//Time taken on a route before swapping to a new patrol route
    public int distanceCheckInterval = 30; //How many frames between running distance checks for how close the player is
    public float goAfterPlayerThreshold = 8.0F; //How close does the player need to be before they go after it
    private int direction = 1;//Integer for the current direction along the patrol the enemy is travelling 1 for forwards, -1 for backwards

    private enum State //Possible states that this enemy can be in
    {
        PATROLLING, //Going between patrol points on the route
        FOLLOWING //Following the player
    }

    private State currentState; //The state this enemy is currently in


    private void Awake()
    {
        agent = gameObject.GetComponent<NavMeshAgent>(); //Get the navmesh agent component
        agent.speed = movementSpeed; //Set the movement speed
        respawnPoint = transform.position;//Set the point that this enemy goes back to when the player dies
        currentState = State.PATROLLING; //Set them to patrol
        ChooseAndSetRandomRoute(); //Choose a random route
    }


    private void ChooseAndSetRandomRoute() //Chooses a random route which isn't the current one
    {
        int numberOfRoutes = routes.Count; //Get the number of routes that this enemy knows about
        PatrolRoute generatedRoute = currentRoute;
        while (currentRoute == generatedRoute)
            generatedRoute = routes[Random.Range(0, numberOfRoutes)]; //Set a random route
        currentRoute = generatedRoute;
        currentPatrolPoint = Random.Range(0, (currentRoute.routeNodes.Count)); //Set them to a point in the route
        agent.SetDestination(currentRoute.routeNodes[currentPatrolPoint].position); //Set their destination
        internalTimer = 0; //Reset the internal timer
    }

    private void Update()
    {
        switch (currentState) //Handle the behaviour dependent on the current state
        {
            case State.PATROLLING:
                internalTimer += Time.deltaTime; //Increment the internal timer
                if (internalTimer >= timeBetweenSwitching) //If they have been on this route for 30 seconds, switch it up
                    ChooseAndSetRandomRoute();

                if (Time.frameCount % distanceCheckInterval == 0) //Check distance to the player and the next patrol point
                {
                    CheckForPlayer();
                    if (Vector3.Distance(transform.position, currentRoute.routeNodes[currentPatrolPoint].position) < 2.0F) //If they are almost at the patrol point, send them to the next one
                    {
                        if (currentPatrolPoint + direction >= currentRoute.routeNodes.Count || currentPatrolPoint + direction < 0) //If that goes over the max or under zero, switch direction
                            direction *= -1;//Multiply the direction by -1 to invert it
                        currentPatrolPoint += direction;
                        agent.SetDestination(currentRoute.routeNodes[currentPatrolPoint].position);//Go to the next point
                    }
                }
                break;
            case State.FOLLOWING:
                if (Time.frameCount % distanceCheckInterval == 0) //Check distance to the player and the next patrol point
                {
                    CheckForPlayer(); //Check how far away the player is to see if they escaped us
                }
                SeekPlayer();//Go after the player
                break;
        }
    }

    private void CheckForPlayer() //Checks how close we are to the player and adjusts the state accordingly
    {
        float distance = GetDistanceToPlayer();
        if (distance <= goAfterPlayerThreshold) //If the player is close enough         
            currentState = State.FOLLOWING;//Start following the player
        else //If the player escapes range, go back to patrolling
        {
            agent.SetDestination(currentRoute.routeNodes[currentPatrolPoint].position);//Go back to the patrol
            currentState = State.PATROLLING;
        }
    }
}