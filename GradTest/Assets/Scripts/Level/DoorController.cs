﻿using System.Collections;
using UnityEngine;

public class DoorController : MonoBehaviour //Script that controls the behaviour of regular doors and the locked one
{
    [Header("Settings")]
    public string doorUserTag = "DoorUser";//Tag given to enemies so that they can use the doors, meaning they will open and close automaticly for them
    public string playerTag = "Player";//Tag given to players so that they can use the doors, meaning they will open and close automaticly for them
    public float doorOpeningSpeed = 4.0F; //Speed at which the door opens or closes
    public bool isLockedDoor = false; //If this is a locked door or not, set to true if it is the final black door, meaning that the player must have the 3 balls in the correct container
    public GameManager gameManager;//Reference to the game manager for the locked door
    private GameObject movingPart; //Reference to the part of the door that actually moves.
    private Vector3 openPosition; //The position the door goes to when it is opened
    private Vector3 closedPosition; //The position the door goes to when it is closed
    private int entitiesInDoorRange = 0; //Number of entities in the trigger of the door
    private bool isOpen = false;
    private void Start()
    {
        movingPart = transform.GetChild(0).gameObject; //Get the moving part which is the child of the door parent gameobject
        closedPosition = movingPart.transform.position;//Get the closed door position
        openPosition = closedPosition + new Vector3(0, 4F, 0); //Create an open door position
    }

    private void OnTriggerEnter(Collider other) //When an object enters the door's trigger box
    {

        if (other.gameObject.CompareTag(doorUserTag) || other.gameObject.CompareTag(playerTag))
        {//Check that it is a door user trying to open the door

            if (isLockedDoor) //Handle the locked door logic seperately
            {
                if (gameManager != null && gameManager.CanFinalDoorUnlock())//check that the game manager reference has actually been set and Check if the door can be unlocked
                {
                    entitiesInDoorRange++;
                }
                else
                {
                    if (other.gameObject.CompareTag(playerTag)) //If the player is trying to get in whilst its locked, show what they're missing
                        gameManager.panelShown = true;

                }

            }
            else //If its not locked, just open
                entitiesInDoorRange++;
        }
    }


    private void OnTriggerExit(Collider other) //When an object exits the door's trigger box
    {
        if (other.gameObject.CompareTag(doorUserTag) || other.gameObject.CompareTag(playerTag)) //Check that it is a door user exiting
            entitiesInDoorRange--;
        if (isLockedDoor && other.gameObject.CompareTag(playerTag)) //Handle the locked door logic seperately
            gameManager.panelShown = false;//Hide the panel
    }

    private void Update()
    {
        if (entitiesInDoorRange > 0 && !isOpen) //If there is at least one entity in range of the door, open it
        {
            StopCoroutine("MoveDoor"); //Stop the door if its already moving (Closing)
            StartCoroutine("MoveDoor", openPosition); //Start opening it
            isOpen = true;
        }
        else if (isOpen && entitiesInDoorRange <= 0) //Else, close it if its open
        {
            StopCoroutine("MoveDoor");
            StartCoroutine("MoveDoor", closedPosition);
            isOpen = false;

        }

    }




    IEnumerator MoveDoor(Vector3 endPos) //Coroutine for sliding the door open and closed
    {
        float t = 0f;
        Vector3 startPos = movingPart.transform.position; //The position the door is starting at
        while (t < 1f)
        {
            t += Time.deltaTime * doorOpeningSpeed;
            movingPart.transform.position = Vector3.Lerp(startPos, endPos, t); //Lerp the doors position
            yield return null;
        }
    }

}
