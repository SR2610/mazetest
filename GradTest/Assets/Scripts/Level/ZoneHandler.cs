﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ZoneHandler : MonoBehaviour //Used for handling the behaviours of the end zone, and the kill zone below the map
{
    [Header("Settings")]
    public GameManager gameManager; //Reference to the gamemanager
    public string playerTag = "Player";//Tag given to the player
    public bool isKillZone = false; //If this is a kill zone
    public bool isEndZone = false; //If this is an end zone

    private void OnTriggerEnter(Collider other) //When an object enters the trigger zone
    {
        if (other.gameObject.CompareTag(playerTag)) //Check if it is the player
        {
            if (isKillZone) //If this is a kill zone, they have gone somewhere they shouldnt
            {
                gameManager.RemovePlayerLife(); //Remove a life from the player for falling out of the map
            }
            if (isEndZone) //End zone is the flat platform at the end of the level
            {
                if (gameManager.CanFinalDoorUnlock()) //Check that they actually have won and haven't got here elseways
                {
                    CrossSceneSettings.didWin = true;//Set the did win variable so that the player sees the correct end scene
                    SceneManager.LoadScene(1);//Load the victory scene
                }
            }
        }
    }

}
