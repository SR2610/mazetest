﻿using UnityEngine;

public class PlayerController : MonoBehaviour //Basic First Person Movement Controller.  Takes Axis inputs and moves the player
{
    [Header("Values")]
    public float movementSpeed = 200F; //Movement Speed of the Player.  Done in units per second, 100 = 1m/s

    private Rigidbody playerBody; //Reference to the player's Rigidbody
    private Vector3 movementDirection; //The direction the player is walking in as determined by the input axis.

    private void Start()
    {
        playerBody = GetComponent<Rigidbody>(); //Get a reference to the players rigidbody when the level starts
    }

    private void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal"); //Get the horizontal input from the controller / keyboard
        float verticalInput = Input.GetAxis("Vertical"); //Get the vertical input from the controller / keyboard

        movementDirection = ((horizontalInput * transform.right) + (verticalInput * transform.forward));  //Set the movement direction to the axis multipled by the direction the player's body is facing
    }

    private void FixedUpdate() //Handle physics related behaviours
    {
        playerBody.velocity = movementDirection * movementSpeed * Time.deltaTime; //Set the rigidbodies velocity to the movement direction * speed * deltaTime (To make it smooth)
    }
}
