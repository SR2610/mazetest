﻿using System.Collections.Generic;
using UnityEngine;

public class PatrolRoute : MonoBehaviour //Script to help with the visualisation and crestion of patrol routes
{
    public Color routeColour = Color.green; //The colour of the route in the Unity Editor
    public List<Transform> routeNodes = new List<Transform>(); //List of the nodes on the route
    private Transform[] nodes; //The transforms of the children of this gameobject

    private void OnDrawGizmos() //Draws a sphere around each point of the patrol and a line between them
    {
        Gizmos.color = routeColour;
        nodes = GetComponentsInChildren<Transform>(); //Gets the children of this gameobject
        routeNodes.Clear();

        foreach (Transform node in nodes)
        {
            if (node != transform) //Check if it isn't itself
            {
                routeNodes.Add(node); //Add it to the route
            }
        }

        for (int i = 0; i < routeNodes.Count; i++) //Draw the route
        {
            Vector3 pos = routeNodes[i].position; //Get the position of the node
            Gizmos.DrawWireSphere(pos, 0.5f); //Draw a sphere around each node
            if (i > 0)
            {
                Vector3 previousPos = routeNodes[i - 1].position;
                Gizmos.DrawLine(previousPos, pos); //Draw a line between the spheres
            }
        }
    }
}
