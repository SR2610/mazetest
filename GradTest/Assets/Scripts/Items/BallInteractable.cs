﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallInteractable : MonoBehaviour //Script placed on balls so that they can be interactable
{
    private Rigidbody ballBody; //Reference to the rigidbody component of the ball

    public GameManager.ObjectColours ballColour; //The colour of the ball.  Used to tell if its been dropped in a correct container
    public bool isHeld = false; //If it is being held right now - Used by the containers to get the player to actually drop it
    private void Awake()
    {
        ballBody = GetComponent<Rigidbody>();//Get the rigidbody when the ball loads
    }

    public void OnPickup(Transform heldPosition) //When the ball is picked up
    {
        ballBody.useGravity = false; //Stop using gravity
        transform.position = heldPosition.position; //Set the position
        transform.parent = heldPosition.parent; //Parent it so it moves with the player
        isHeld = true;
    }

    public void OnDrop() //When the ball is dropped
    {
        transform.parent = null; //Remove the parent
        ballBody.useGravity = true; //Re-enable gravity
        isHeld = false;
    }

}
