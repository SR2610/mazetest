﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour //Basic First Person Camera Script
{
    [Header("Camera Movement Settings")] //Multiple settings for the camera controls
    public float minX = -60f; //How far in degrees can the player look down
    public float maxX = 60f; //How far in degrees can the player look up - These are not needed for the Y axis as the player can completely turn around
    public float sensitivityX = 15f; //The sensitivity of the mouse look
    public float sensitivityY = 15f;

    [Header("Object References")]
    public Camera cameraRef; //Reference to the camera childed to the player

    private float rotationX = 0f; //The current rotation of the player
    private float rotationY = 0f;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //Lock the cursor to the centre of the screen and hide it at the start of the game
    }

    private void Update()
    {
        rotationY += Input.GetAxis("Mouse X") * sensitivityY; //Get the horizontal mouse input
        rotationX += Input.GetAxis("Mouse Y") * sensitivityX; //Get the vertical mouse input
        rotationX = Mathf.Clamp(rotationX, minX, maxX); //Clamp the X Axis (vertical) rotation to the minimum and maximum values
        transform.localEulerAngles = new Vector3(0, rotationY, 0); //Rotate the player around the y axis (horizontal) for where they are looking
        cameraRef.transform.localEulerAngles = new Vector3(-rotationX,0, 0); //Set the cameras vertical rotation, horizontal rotation is handled via being parented to the above spinning player

        if (Input.GetButtonDown("Cancel")) //If the player presses escape
        {
            Cursor.lockState = CursorLockMode.None; //Unlock the cursor from the middle of the screen
            Cursor.visible = true; //Show the cursor
        }
        if (Input.GetButtonDown("Click")) //When the player clicks back onto the game
        {
            Cursor.lockState = CursorLockMode.Locked; //Hide and lock the cursor
        }
    }
}
